﻿using System;
using System.Data.SqlClient;

namespace SQLAccess
{
    class Program
    {
        static void Main(string[] args)
        {
            SqlConnection connection;
            SqlCommand command;
            string sql = null;
            SqlDataReader dataReader;
            sql = "select color from colors";
            // connection = new SqlConnection("Data Source=MANGO;Initial Catalog=Ruud;Integrated Security=SSPI;");
            connection = new SqlConnection("Server=MANGO;Database=Ruud;Trusted_Connection=Yes;");
            try
            {
                connection.Open();
                Console.WriteLine("Connection opened");
                command = new SqlCommand(sql, connection);
                dataReader = command.ExecuteReader();
                int i = 1;
                while (dataReader.Read())
                {
                    Console.WriteLine(i++ + " " + dataReader.GetValue(0));
                }
                dataReader.Close();
                command.Dispose();
                connection.Close();
            }
            catch (Exception e)
            {
                Console.WriteLine("Can not open connection "+e.GetBaseException());
            }
            finally
            {
                Console.ReadKey();
            }
        }
    }
}
